﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace SSEDemo.Helpers
{
    static class WebPageHelper
    {
        public const int ShortWait = 10;

        /// <summary>
        /// Peform a Click on Webelement 
        /// </summary>
        /// <param name="element">Webelement to action </param>
        public static void clickOnelement(this IWebElement element)
        {
            waitForElementToBeClickable(element);
            element.Click();
        }

        /// <summary>
        /// Clear and Enter text in Textbox
        /// </summary>
        /// <param name="element">Webelement for action</param>
        /// <param name="text">Value</param>
        public static void sendText(this IWebElement element, String text)
        {
            element.Clear();
            element.SendKeys(text);
        }

        /// <summary>
        /// Returns the Text for Webelement
        /// </summary>
        /// <param name="element">Webelement for action</param>
        /// <returns>Text value of Webelement</returns>
        public static string getText(this IWebElement element)
        {
            return element.Text;
        }

        /// <summary>
        /// Wait until Webelement is Clickable
        /// </summary>
        /// <param name="element">Webelement for action</param>
        public static void waitForElementToBeClickable(IWebElement element)
        {
            Utility.getWaitDriver(ShortWait).Until(ExpectedConditions.ElementToBeClickable(element));
        }

        /// <summary>
        /// Wait until Webelement is Selectable
        /// </summary>
        /// <param name="element">Webelement for action</param>
        public static void waitForElementToBeSelected(IWebElement element)
        {
            Utility.getWaitDriver(ShortWait).Until(ExpectedConditions.ElementToBeSelected(element));
        }
    }
}
