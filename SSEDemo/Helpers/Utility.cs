﻿using System;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace SSEDemo.Helpers
{
    class Utility
    {
        public static string hompageUrl = "https://sse.co.uk";
        public static IWebDriver getDriver { get; set; }

        /// <summary>
        /// Initilaise the IWait
        /// </summary>
        /// <param name="waitTime">Time to wait in seconds</param>
        /// <returns>IWait</returns>
        public static IWait<IWebDriver> getWaitDriver(int waitTime)
        {
            IWait<IWebDriver> wait = new WebDriverWait(getDriver, TimeSpan.FromSeconds(waitTime));
            return wait;
        }

        /// <summary>
        /// Navigate to homepage
        /// </summary>
        public static void goToHomePage()
        {
            getDriver.Manage().Window.Maximize();
            navigateTo(hompageUrl); 
        }

        /// <summary>
        /// Naviagte to provided URL
        /// </summary>
        /// <param name="url">URL</param>
        public static void navigateTo(String url)
        {
            getDriver.Navigate().GoToUrl(url);
        }

        /// <summary>
        /// Returns the path for file in resources folder 
        /// </summary>
        /// <param name="filename">Name of file</param>
        /// <returns></returns>
        internal static string getExcelPathFromResources(string filename)
        {
            String baseDirectory = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            return Directory.GetParent(Directory.GetParent(baseDirectory).FullName).FullName + "/Resources/" + filename;
        }
    }

    
    enum TariffType
    {
        Standard,
        SSE_1_Year_Fix_V10,
        Fix_2020,
        Fix_And_Shop_V3,
    }
}
