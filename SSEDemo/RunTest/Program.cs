﻿using System;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using SSEDemo.Helpers;

namespace SSEDemo
{
    class Program
    {

        static void Main(string[] args)
        {
        }

        [SetUp]
        public void setUp()
        {
            Utility.getDriver = new ChromeDriver();
            Utility.goToHomePage();
        }

        [Test]
        public void executeTest()
        {
            int recordRowCount = ReadExcel.createCollectionFromExcel(Utility.getExcelPathFromResources("SSEExcel.xlsx"));
            for (int currentRow = 1; currentRow <= recordRowCount; currentRow++) {
                MegaMenuPO energyPage = new MegaMenuPO();
                OurPricePO ourPricePage = energyPage.navigateToPriceAndTariff();
                ViewTariffPO viewTariffPage = ourPricePage.searchPostCode(ReadExcel.ReadData(currentRow, "PostCode"));
                viewTariffPage.chooseATariff(TariffType.Fix_2020);
                String standingCharge = viewTariffPage.getStandingCharge(TariffType.Fix_2020);
                Assert.AreEqual(ReadExcel.ReadData(currentRow, "ExpecetedStandingCharges"), standingCharge);
            }
        }

        [TearDown]
        public void tearDown()
        {
            Utility.getDriver.Quit();
        }
    }
}
