﻿using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SSEDemo.Helpers;

namespace SSEDemo
{
    class ViewTariffPO
    {
        public ViewTariffPO()
        {
            PageFactory.InitElements(Utility.getDriver, this);
        }

        [FindsBy(How = How.Id, Using = "tariffs-button")]
        public IWebElement tariffsButton { get; set; }

        [FindsBy(How = How.Id, Using = "ui-id-20")]
        public IWebElement fix2020List { get; set; }

        [FindsBy(How = How.XPath, Using = "//h3[.='Fix 2020']/ ..//th[.='Direct Debit and paperless bills']/ ../td[2]")]
        public IWebElement fix2020StandingChargeDDAndPB{ get; set; }

        public void chooseATariff(TariffType tariffSelection)
        {
            Thread.Sleep(10000);
            tariffsButton.clickOnelement();
            switch (tariffSelection)
            {
                case TariffType.Standard:
                    break;
                case TariffType.SSE_1_Year_Fix_V10:
                    break;
                case TariffType.Fix_2020:
                    fix2020List.Click();
                    break;
                case TariffType.Fix_And_Shop_V3:
                    break;
            }
        }

        public string getStandingCharge(TariffType tariffSelection)
        {
            string standingCharge = "";
            switch (tariffSelection)
            {
                case TariffType.Standard:
                    break;
                case TariffType.SSE_1_Year_Fix_V10:
                    break;
                case TariffType.Fix_2020:
                    standingCharge = fix2020StandingChargeDDAndPB.getText();
                    break;
                case TariffType.Fix_And_Shop_V3:
                    break;
            }
            return standingCharge;
        }
    }
}
