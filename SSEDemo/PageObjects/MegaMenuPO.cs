﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SSEDemo.Helpers;

namespace SSEDemo
{
    class MegaMenuPO
    {
        public MegaMenuPO() {
            PageFactory.InitElements(Utility.getDriver, this);
        }

        [FindsBy(How = How.LinkText, Using = "Energy")]
        public IWebElement energyLinkText { get; set; }

        [FindsBy(How = How.LinkText, Using = "Prices and tariff information")]
        public IWebElement priceAndTariffLinkText { get; set; }

        public OurPricePO navigateToPriceAndTariff()
        {
            energyLinkText.clickOnelement();
            priceAndTariffLinkText.clickOnelement();
            return new OurPricePO();
        }
    }
}
