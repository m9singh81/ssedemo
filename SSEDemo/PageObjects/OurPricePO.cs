﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SSEDemo.Helpers;

namespace SSEDemo
{
    class OurPricePO
    {
        public OurPricePO()
        {
            PageFactory.InitElements(Utility.getDriver, this);
        }

        [FindsBy(How = How.Id, Using = "postcode")]
        public IWebElement postCodeInput { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".action .button .next")]
        public IWebElement viewTariffsButton { get; set; }

        public ViewTariffPO searchPostCode(string postCode)
        {
            postCodeInput.sendText(postCode);
            viewTariffsButton.clickOnelement();
             return new ViewTariffPO();
        }
    }
}
